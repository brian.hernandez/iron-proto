﻿using UnityEngine;

public class Mover : MonoBehaviour
{
  public bool useFixed = false;

  [Space]
  public bool useLocalTranslation = false;
  public Vector3 translate = Vector3.zero;

  [Space]
  public bool useLocalRotation = false;
  public Vector3 rotate = Vector3.zero;

  private void Update()
  {
    if (!useFixed)
      Translate();
  }

  private void FixedUpdate()
  {
    if (useFixed)
      Translate();     
  }

  private void Translate()
  {
    transform.Translate(translate * Time.deltaTime,
      useLocalTranslation ? Space.Self : Space.World);

    transform.Rotate(rotate * Time.deltaTime,
      useLocalRotation ? Space.Self : Space.World);
  }
}
